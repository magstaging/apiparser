<?php

namespace Mbs\Calculator\Api;

interface OperatorInterface
{
    /**
     * @return \Mbs\Calculator\Api\Data\ParsedDataInterface
     */
    public function parse();
}
