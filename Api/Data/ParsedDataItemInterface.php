<?php


namespace Mbs\Calculator\Api\Data;

interface ParsedDataItemInterface
{
    /**
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getValue();
}
