<?php

namespace Mbs\Calculator\Api\Data;

interface ParsedDataInterface
{
    /**
     * Return localized parameters for the application
     *
     * @return \Mbs\Calculator\Api\Data\ParsedDataItemInterface[]
     */
    public function getLocalization();
}
