<?php

namespace Mbs\Calculator\Model;

use Mbs\Calculator\Api\OperatorInterface;

class Operator implements OperatorInterface
{
    /**
     * @var \Mbs\Calculator\Api\Data\ParsedDataInterfaceFactory
     */
    private $parsedDataInterfaceFactory;

    public function __construct(
        \Mbs\Calculator\Api\Data\ParsedDataInterfaceFactory $parsedDataInterfaceFactory
    ) {
        $this->parsedDataInterfaceFactory = $parsedDataInterfaceFactory;
    }

    /**
     * @return \Mbs\Calculator\Api\Data\ParsedDataInterface
     */
    public function parse()
    {
        $data = [
            [
                'row_id' => 1,
                'option_label' => 'tmp_dir',
                'option_value' => 'temp',
                'position' => 1,
                'record_id' => 0,
                'initialize' => true
           ],
           [
                'row_id' => 2,
                'option_label' => 'main_dir',
                'option_value' => 'APD',
                'position' => 2,
                'record_id' => 1,
                'initialize' => true
            ]
        ];

        $output = [];
        foreach ($data as $param) {
            $output[$param['option_label']] =  $param['option_value'];
        }

        return $this->parsedDataInterfaceFactory->create([
            'itemsData' => $output
        ]);
    }
}
