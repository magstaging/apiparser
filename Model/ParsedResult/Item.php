<?php

namespace Mbs\Calculator\Model\ParsedResult;

use Mbs\Calculator\Api\Data\ParsedDataItemInterface;

class Item implements ParsedDataItemInterface
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $value;

    /**
     * @inheritdoc
     */
    public function __construct(
        string $label,
        string $value
    ) {
        $this->label = $label;
        $this->value = $value;
    }

    /**
     * @inheritdoc
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function getValue()
    {
        return $this->value;
    }
}
